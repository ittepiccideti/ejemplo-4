package com.androidbeginners.israelarjonavizcaino.ejemplo4;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class ConversionActivity extends AppCompatActivity {

    EditText cantidad;
    Spinner tipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);

        cantidad = (EditText) findViewById(R.id.etCantidad);
        tipo = (Spinner) findViewById(R.id.spTipo);
    }

    public void convertir(View v) {
        try {
            Double can = Double.parseDouble(cantidad.getText().toString());
            int opcion = tipo.getSelectedItemPosition();

            switch (opcion) {
                case 0:
                    can *= 22.49;
                    break;
                case 1:
                    can *= 20.76;
                    break;
            }

            AlertDialog.Builder mensaje = new AlertDialog.Builder(this);
            mensaje.setTitle("Resultado");
            mensaje.setMessage("La conversión es: "+can);
            mensaje.show();

            cantidad.setText("");
        } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.item1:
                Toast.makeText(getApplicationContext(), "Aquí estas", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item2:
                Toast.makeText(getApplicationContext(), "Item 2 Selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item3:
                Toast.makeText(getApplicationContext(), "Saliendo", Toast.LENGTH_SHORT).show();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
